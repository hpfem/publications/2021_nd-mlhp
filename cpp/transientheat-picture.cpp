// MIT License, Copyright (c) 2022, Philipp Kopp (philipp.kopp@tum.de)

#include "transientheat.hpp"

int main( )
{
    using namespace mlhp;
    
    static constexpr size_t D = 3;

    auto tS = utilities::tic( );

    // Physics setup
    double duration = 1.0;
    double sigma = 0.0021;
    double u0 = 25.0;

    double capacity = 1.0;
    auto conductivity = 0.01;

    std::array<double, 3> lengths { 1.0, 0.4, 0.1 };

    std::function path = linear_am::sourcePosition;

    double power = 0.968;

    std::function intensity = [=]( double t ) noexcept
    {
        return power * std::min( t / 0.05, 1.0 );
    };

    spatial::ScalarFunction<D + 1> source = solution::amLinearHeatSource( path, intensity, sigma );

    spatial::ScalarFunction<D + 1> solution = solution::amLinearHeatSolution( path,
        intensity, capacity, conductivity, sigma, duration / 100, u0 );

    auto initialCondition = spatial::constantFunction<D>( u0 );

    auto boundaries = boundary::allFaces( D );

    boundaries.pop_back( );

    // Discretization parameters
    size_t nsteps = 256;
    double theta = 0.5;

    double dt = duration / nsteps;

    std::array<size_t, 3> nelements = { 10, 4, 1 };

    PolynomialDegreeVector degrees { 5, 5, 4, 3, 3, 2 };

    auto grading = PerLevelGrading { degrees };

    using AnsatzSpace = TrunkSpace;

    // Change to 1 to postprocess every time step
    size_t vtuInterval = 8;

    // Initial discretization
    auto basis0 = linear_am::discretize<AnsatzSpace>( nelements, lengths, 0.0, grading );

    auto dofs0 = projectOnto( *basis0, initialCondition );

    // Assembly stuff
    auto orderDeterminor = makeIntegrationOrderDeterminor<D>( 1 );
    auto partitioner = NoPartitioner<D> { };
    auto solve = linalg::makeCGSolver( 1e-6 );

    // Postprocess initial condition (although zero)
    linear_am::writeSolutionToVtu( *basis0, dofs0, 0, vtuInterval, 0.0, solution, source );

    // Error in initial condition 
    //auto norms = ErrorIntegrals{ };
    auto ndof = DofIndex { 0 }; 

    size_t gridMemory = 0, basisMemory = 0, matrixMemory = 0;
    double discretizeTime = 0.0, boundaryTime = 0.0, assemblyTime = 0.0, solveTime = 0.0;

    // Time stepping
    for( size_t iTimeStep = 0; iTimeStep < nsteps; ++iTimeStep )
    {
        double time0 = iTimeStep * dt;
        double time1 = ( iTimeStep + 1 ) * dt;

        // Create discetisation for t^{i+1}
        auto t0 = utilities::tic( );

        auto basis1 = linear_am::discretize<AnsatzSpace>( nelements, lengths, time1, grading );

        std::cout << "Time step " << iTimeStep + 1 << " / " << nsteps;
        std::cout << " (" << basis1->ndof( ) << " number of unknowns)" << std::endl;
 
        // Compute boundary dofs
        auto t1 = utilities::tic( );

        auto function = spatial::sliceLast( solution, time1 );

        auto dirichletDofs = boundary::boundaryDofs<D>( function, *basis1, boundaries );

        // Assemble linear system
        auto t2 = utilities::tic( );
        auto matrix = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis1, dirichletDofs.first );
        auto rhs = std::vector<double>( matrix.size1( ), 0.0 );

        auto integrand = linear_am::makeIntegrand<D>( dofs0, source, 
                capacity, conductivity, theta, time0, time1 );

        integrateOnDomain( *basis0, *basis1, integrand, { matrix, rhs }, dirichletDofs );

        // Solve linear system
        auto t3 = utilities::tic( );
        auto dofs1 = boundary::inflate( solve( matrix, rhs ), dirichletDofs );

        // Vtu postprocessing
        auto t4 = utilities::tic( );
 
        linear_am::writeSolutionToVtu( *basis1, dofs1, iTimeStep + 1, vtuInterval, time1, solution, source );

        // Error integral
        //double weight = iTimeStep + 1 < nsteps ? dt : dt / 2.0;
        //linear_am::addL2ErrorIntegral( *basis1, solution, dofs1, time1, weight, norms );

        ndof += basis1->ndof( );
        gridMemory += basis1->mesh( ).memoryUsage( );
        basisMemory += basis1->memoryUsage( );
        matrixMemory += matrix.memoryUsage( );
        discretizeTime += utilities::seconds( t0, t1 );
        boundaryTime += utilities::seconds( t1, t2 );
        assemblyTime += utilities::seconds( t2, t3 );
        solveTime += utilities::seconds( t3, t4 );

        // Move discretization for next step
        dofs0 = dofs1;
        basis0 = basis1;
    }

    double totalTime = utilities::seconds( tS, utilities::tic( ) );

    std::cout << "Total runtime  : " << totalTime      << "s (" << totalTime      / nsteps << "s per step)" << std::endl;
    std::cout << "    discretize : " << discretizeTime << "s (" << discretizeTime / nsteps << "s per step)" << std::endl;
    std::cout << "    boundary   : " << boundaryTime   << "s (" << boundaryTime   / nsteps << "s per step)" << std::endl;
    std::cout << "    assembly   : " << assemblyTime   << "s (" << assemblyTime   / nsteps << "s per step)" << std::endl;
    std::cout << "    solve      : " << solveTime      << "s (" << solveTime      / nsteps << "s per step)" << std::endl;
    std::cout << "Average number of dofs: " << ndof / static_cast<double>( nsteps ) << std::endl;
    std::cout << "Average grid memory: " << gridMemory / static_cast<double>( nsteps ) << " bytes" << std::endl;
    std::cout << "Average basis memory: " << basisMemory / static_cast<double>( nsteps ) << " bytes" << std::endl;
    std::cout << "Average matrix memory: " << matrixMemory / static_cast<double>( nsteps ) << " bytes" << std::endl;
    // std::cout << "Solution in L2 norm: " << norms.analytical( ) << std::endl;
    // std::cout << "Relative error in L2 norm: " << norms.relativeDifference( ) * 100 << " %" << std::endl;
}

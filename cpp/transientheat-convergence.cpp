// MIT License, Copyright (c) 2022, Philipp Kopp (philipp.kopp@tum.de)

#include "transientheat.hpp"

template<typename AnsatzSpace>
void linearAMTimeStepping( size_t nsteps, size_t degree, double theta, std::string studyString )
{
    using namespace mlhp;

    static constexpr size_t D = 3;

    // Physics setup
    double duration = 1.0;
    double sigma = 0.0021;
    double u0 = 25.0;

    double capacity = 1.0;
    auto conductivity = 0.01;

    std::array<double, 3> lengths { 1.0, 0.4, 0.1 };

    std::function path = linear_am::sourcePosition;

    double power = 0.968;

    std::function intensity = [=]( double t ) noexcept
    {
        return power * std::min( t / 0.05, 1.0 );
    };

    spatial::ScalarFunction<D + 1> source = solution::amLinearHeatSource( path, intensity, sigma );

    spatial::ScalarFunction<D + 1> solution = solution::amLinearHeatSolution( path,
         intensity, capacity, conductivity, sigma, duration / 100, u0 );

    auto initialCondition = spatial::constantFunction<D>( u0 );

    auto boundaries = boundary::allFaces( D );

    boundaries.pop_back( );

    // Discretization parameters
    auto nelements = std::array<size_t, 3>{ 10, 4, 1 };
    auto grading = UniformGrading { degree };

    // Initial discretization
    auto basis0 = linear_am::discretize<AnsatzSpace>( nelements, lengths, 0.0, grading );

    auto dofs0 = projectOnto( *basis0, initialCondition );

    //// Postprocess initial condition
    //size_t vtuInterval = nsteps;
    //linear_am::writeSolutionToVtu( *basis0, dofs0, 0, vtuInterval, 0.0, solution, source );

    // Assembly stuff
    auto orderDeterminor = makeIntegrationOrderDeterminor<D>( 1 );
    auto partitioner = NoPartitioner<D> { };
    auto solve = linalg::makeCGSolver( );

    double dt = duration / nsteps;

    // Error in initial condition 
    auto norms = ErrorIntegrals { };
    auto ndof = basis0->ndof( );

    linear_am::addL2ErrorIntegral( *basis0, solution, dofs0, 0.0, dt / 2.0, norms );

    // Time stepping
    for( size_t iTimeStep = 0; iTimeStep < nsteps; ++iTimeStep )
    {
        double time0 = iTimeStep * dt;
        double time1 = ( iTimeStep + 1 ) * dt;

        // Create discetisation for t^{i+1}
        auto basis1 = linear_am::discretize<AnsatzSpace>( nelements, lengths, time1, grading );

        std::cout << "Time step " << iTimeStep + 1 << " / " << nsteps;
        std::cout << " (" << basis1->ndof( ) << " number of unknowns)" << std::endl;

        // Compute boundary dofs
        auto function = spatial::sliceLast( solution, time1 );

        auto dirichletDofs = boundary::boundaryDofs<D>( function, *basis1, boundaries );

        // Assemble linear system
        auto matrix = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis1, dirichletDofs.first );
        auto rhs = std::vector<double>( matrix.size1( ), 0.0 );

        auto integrand = linear_am::makeIntegrand<D>( dofs0, source,
            capacity, conductivity, theta, time0, time1 );

        integrateOnDomain( *basis0, *basis1, integrand, { matrix, rhs }, dirichletDofs );

        // Solve linear system
        auto dofs1 = boundary::inflate( solve( matrix, rhs ), dirichletDofs );

        //// Vtu postprocessing
        //linear_am::writeSolutionToVtu( *basis1, dofs1, iTimeStep + 1, vtuInterval, time1, solution, source );

        // Error integral
        double weight = iTimeStep + 1 < nsteps ? dt : dt / 2.0;

        ndof += basis1->ndof( );

        linear_am::addL2ErrorIntegral( *basis1, solution, dofs1, time1, weight, norms );

        // Move discretization for next step
        dofs0 = dofs1;
        basis0 = basis1;
    }

    std::cout << studyString << ndof << ", " << std::scientific << std::setprecision( 12 ) 
              << norms.numerical( ) << ", "
              << norms.analytical( ) << ", " 
              << norms.relativeDifference( ) << std::endl;
}

// Parse input string and call linearAMTimeStepping
int main( int argc, char** argv )
{
    std::string usage = 
        "Pass the configuration parameters without space, for example CN08Tr05 will use "
        "Crank-Nicolson (CN) with 2^8 (08) time steps and Trunk space (Tr) with p = 5 (05) "
        "on each element. CN08 and Tr05 can be swapped. Other options are:\n"
        "    CN -> BE (Backward-Euler)\n"
        "    Tr -> Te (Tensor space).";

    MLHP_CHECK( argc == 2, "Invalid number of arguments. " + usage ); 

    std::string study = argv[1];

    if( study.size( ) != 8 )
    {
        std::cout << "Invalid study string. " + usage;

        return 0;
    }

    // Split into time and space discretization
    std::string timeType = study.substr( 0, 2 );
    std::string spaceType = study.substr( 4, 2 );

    auto timeIndex = static_cast<size_t>( std::stoi( study.substr( 2, 2 ) ) );
    auto spaceIndex = static_cast<size_t>( std::stoi( study.substr( 6, 2 ) ) );
    
    // Swap if needed
    if( timeType == "Tr" || timeType == "Te" )
    {
        std::swap( timeType, spaceType );
	    std::swap( timeIndex, spaceIndex );
    }

    if( ( timeType  != "BE" && timeType  != "CN" )  ||
        ( spaceType != "Tr" && spaceType != "Te" ) )
    {
        std::cout << "Invalid study string. " + usage;

        return 0;
    }

    double theta = timeType == "CN" ? 0.5 : 1.0;

    auto nsteps = mlhp::utilities::integerPow<size_t>( 2, timeIndex );
    
    std::string str = "tstudy, nsteps, pstudy, sum dofs, numerical norm, analytical norm, relative difference norm\n";
    str += std::to_string( timeIndex ) + ", " + std::to_string( nsteps ) + ", " + std::to_string( spaceIndex ) + ", ";

    std::cout << "Study: " << study << ", nsteps = " << nsteps << ", theta = " << theta << ", p = " << spaceIndex << std::endl;

    if( spaceType == "Te" )
    {
        linearAMTimeStepping<mlhp::TensorSpace>( nsteps, spaceIndex, theta, str );
    }
    else
    {
        linearAMTimeStepping<mlhp::TrunkSpace>( nsteps, spaceIndex, theta, str );
    }
}

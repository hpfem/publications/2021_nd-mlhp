// MIT License, Copyright (c) 2022, Philipp Kopp (philipp.kopp@tum.de)

#include "mlhp/core.hpp"

#include <iomanip>

namespace mlhp::linear_am
{

struct Refinement
{
    double timeDelay;
    double sigma;
    double refinementLevel;
};

auto evaluateRefinements( const auto& refinements, double delay, double distance )
{
    for( size_t irefinement = 0; irefinement + 1 < refinements.size( ); ++irefinement )
    {
        auto& refinement0 = refinements[irefinement];
        auto& refinement1 = refinements[irefinement + 1];

        if( delay < refinement1.timeDelay )
        {
            // Map into current refinement segment
            auto tau = utilities::mapToLocal0( refinement0.timeDelay, refinement1.timeDelay, delay );

            // Interpolate sigma and level in current refinement segment
            auto sigma = ( 1.0 - tau ) * refinement0.sigma + tau * refinement1.sigma;
            auto maxlevel = ( 1.0 - tau ) * refinement0.refinementLevel + tau * refinement1.refinementLevel;

            // Evaluate exponential
            auto level = maxlevel * std::exp( -distance * distance / ( 2.0 * sigma * sigma ) );
            
            return static_cast<RefinementLevel>( std::round( level ) );
        }
    }

    return RefinementLevel { 0 };
}

auto sourcePosition( double time )
{
    return std::array { ( 1.0 - time ) * 0.2 + time * 0.8, 0.2, 0.1 };
}

template<typename AnsatzSpace, size_t D>
auto discretize( std::array<size_t, D> nelements,
                 std::array<double, D> lengths,
                 double time,
                 auto grading )
{
    auto grid0 = makeRefinedGrid( nelements, lengths );

    std::vector refinements =
    {
        Refinement { 0.0, 0.028, 3.65 },
        Refinement { 0.15, 0.05, 1.3 },
        Refinement { 0.5, 0.10, 0.5 },
    };

    auto levelFunction = [=](std::array<double, 3> xyz)
    {
        if( time == 0.0 ) return RefinementLevel { 0 };

        auto [p, t] = spatial::closestPointOnSegment( sourcePosition( 0.0 ), sourcePosition( time ), xyz);

        return evaluateRefinements( refinements, time * ( 1.0 - t ), spatial::norm( p - xyz ) );
    };

    auto levelRefinement = refineWithLevelFunction<D>( levelFunction );
    auto sourceRefinement = refineInsideDomain( implicit::sphere( sourcePosition( time ), 0.005 ), 5 );

    grid0->refine( refinementOr( levelRefinement, sourceRefinement ) );

    return makeHpBasis<AnsatzSpace>( grid0, grading );
}

template<size_t D>
auto makeIntegrand( const std::vector<double>& dofs0,
                    const spatial::ScalarFunction<D + 1>& source,
                    double capacity, double conductivity, 
                    double theta, double time0, double time1  )
{
    auto capacityFunction = spatial::constantFunction<D + 1>( capacity );
    auto conductivityFunction = spatial::constantFunction<D + 1>( conductivity );

    return makeTransientPoissonIntegrand<D>( capacityFunction, conductivityFunction,
        source, dofs0, { time0, time1 }, theta );
}

template<size_t D>
auto writeSolutionToVtu( const MultilevelHpBasis<D>& basis,
                         const std::vector<double>& dofs,
                         size_t iStep, size_t interval, double time,
                         const spatial::ScalarFunction<D + 1>& solution,
                         const spatial::ScalarFunction<D + 1>& source )
{
    if( iStep % interval == 0 )
    {
        std::vector postprocessors
        { 
            makeSolutionPostprocessor<D>( dofs, 1 ), 
            makeFunctionPostprocessor<D>( spatial::sliceLast( solution, time ), "Solution" ),
            makeFunctionPostprocessor<D>( spatial::sliceLast( source, time ), "Source" )
        };

        // Sub-samples per element
        auto fullName = "outputs/linear_am_" + std::to_string( ( iStep / interval ) + 1 );
        auto nsamples = array::setEntry( array::make<size_t, D>( 6 ), D - 1, size_t { 2 } );

        postprocess( nsamples, postprocessors, basis, fullName, 1 );
    }
}

template<size_t D>
void addL2ErrorIntegral( const MultilevelHpBasis<D>& basis,
                         const spatial::ScalarFunction<D + 1>& solution,
                         const std::vector<double>& dofs,
                         double time, double weight, 
                         ErrorIntegrals& norms )
{
    auto errorIntegrand = makeL2ErrorIntegrand<D>( dofs, spatial::sliceLast( solution, time ) );
    auto errorsOfStep = ErrorIntegrals { };

    integrateOnDomain( basis, errorIntegrand, errorsOfStep );

    norms.numericalSquared += weight * errorsOfStep.numericalSquared;
    norms.analyticalSquared += weight * errorsOfStep.analyticalSquared;
    norms.differenceSquared += weight * errorsOfStep.differenceSquared;
}

} // namespace mlhp::linear_am


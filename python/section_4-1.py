# MIT License, Copyright (c) 2022, Philipp Kopp (philipp.kopp@tum.de)

import mlhp
import numpy
import scipy.sparse.linalg
import matplotlib.pyplot as plt

def compute(D, depth, grading, maskInitializer, writeResultFiles):
    
    # Refine last element of each level until we reach depth 
    mesh = mlhp.Grid([2] * D)
    
    for level in range(depth):
        mesh.refine([max(mesh.ncells( ) - 2**D, 0)])
        
    # Determine polynomial degrees and create multi-level hp basis
    levels = [mesh.cells[i].level for i in mesh.fullIndices]
    degrees = [[grading(l, depth)] * D for l in levels]
    
    basis = mlhp.Basis(mesh, degrees, maskInitializer)
    
    # Compute domain finite element integral
    conductivity = lambda xyz : 1.0
    sourceFunction = lambda xyz :  (3 - 2 * D) / 4 * numpy.sum(xyz**2, axis=0)**(-3 / 4)

    K, F = mlhp.integratePoissonSystem(basis, conductivity, sourceFunction)

    # Impose Dirichlet boundary conditions
    solution = lambda xyz : numpy.sum(xyz**2, axis=0)**(1/4)
    faces = mlhp.boundaryFaces(basis, [(axis, 1) for axis in range(D)])
    dirichlet = mlhp.projectOnFaces(basis, faces, solution)

    K, F = mlhp.constrainStrongly(K, F, dirichlet)
    
    # Solve linear system  
    def niter(u):
        niter.n += 1
    niter.n = 0
    
    interior = scipy.sparse.linalg.cg(K, F, tol=1e-10, M=mlhp.invdiag(K), callback=niter)[0]
    dofs = mlhp.expandDirichlet(interior, dirichlet)
    
    # Write solution to vtu file for visualization in Paraview
    if D <= 3 and writeResultFiles:
        postprocessors = [mlhp.makeSolutionPostprocessor(dofs, "NumericalSolution"),
                          mlhp.makeFunctionPostprocessor(solution, "AnalyticalSolution")]
                          
        mlhp.postprocessVtu(basis, postprocessors, "singular_" + str(D) + "D_" + str(depth) + ".vtu")

    # Integrate error
    analyticalDerivatives = lambda xyz : 0.5 * xyz / numpy.sum(xyz**2, axis=0)**(3 / 4)

    energyNorms = mlhp.integrateEnergyError(basis, dofs, analyticalDerivatives, mlhp.pplus(3))
    l2Norms = mlhp.integrateL2Error(basis, dofs, solution, mlhp.pplus(3))
    
    return len(interior), l2Norms[2] / l2Norms[0], energyNorms[2] / l2Norms[0], K.nnz, niter.n

# Run convergence study
linearGrading = lambda L, maxL: maxL - L + 1
uniformGrading = lambda L, maxL: maxL + 1

trunkSpace = mlhp.trunkspace
tensorSpace = mlhp.tensorspace

ndim = 3
nrefinements = 7
writeResultFiles = True
ndof, l2Errors, energyErrors = [], [], []

print("Results:   (#dofs, l2 error, energy error, nonzeros, iterations)")

for i in range(nrefinements):
    data = compute(ndim, i, linearGrading, trunkSpace, writeResultFiles)

    print("i = " + str(i + 1) + " / " + str(nrefinements) + ": " + str(data), flush=True)
      
    ndof.append(data[0])
    l2Errors.append(data[1])
    energyErrors.append(data[2])
    
# Plot results
x = numpy.array(ndof)**(1/(ndim + 1))

plt.semilogy(x, l2Errors, 'bx-')
plt.semilogy(x, energyErrors, 'rx-')
plt.xlabel('(#dofs)^(1/' + str(ndim + 1) + ')')
plt.ylabel('Relative error [%]')
plt.legend(['L2 norm', 'Energy norm'])
plt.show()

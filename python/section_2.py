# MIT License, Copyright (c) 2022, Philipp Kopp (philipp.kopp@tum.de)

import mlhp, numpy

# Create mesh and extract neighbors
mesh = mlhp.Grid([4, 3])

A = numpy.array([cell.neighbors for cell in mesh.cells])

# Polynomial degrees for each element
p = numpy.array([[2, 2], [2, 1], [3, 2],  
                 [2, 3], [1, 2], [3, 1],  
                 [2, 2], [1, 1], [2, 1],  
                 [3, 3], [2, 3], [2, 2]])
                 
# Construct tensor-product masks and location matrices
M = mlhp.createPfemMasks(A, p, mlhp.tensorspace, 'min_degree')
G = mlhp.createPfemLocationMatrices(M, A)

ndof = numpy.max([numpy.max(Gi.ravel()) for Gi in G if Gi.size]) + 1

print("Tensor-product masks:")
for Mi in M:
    print(Mi.T[::-1].astype(int))

print("\nLocation matrices (" + str(ndof) + " dofs):")
for Gi in G:
    print(Gi.T[::-1])
    
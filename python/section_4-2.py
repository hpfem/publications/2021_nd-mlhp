# MIT License, Copyright (c) 2022, Philipp Kopp (philipp.kopp@tum.de)

import mlhp
import numpy
import scipy.sparse.linalg

# Physical setup
capacity = 1
conductivity = 0.01

duration = 1.0
power = 0.948
sigma = 0.0021
u0 = 25

sourcePosition = lambda t : numpy.array([(1 - t) * 0.2 + t * 0.8, 0*t + 0.2, 0*t + 0.1]);                                  
sourceIntensity = lambda t : power * numpy.minimum(t / 0.05, 1);

capacityFunction = lambda xyz : capacity
conductivityFunction = lambda xyz : conductivity

# Discretization
nsteps = 256
theta = 0.5;

lengths = [1.0, 0.4, 0.1];
nelements = [10, 4, 1];

degreesPerLevel = [5, 5, 4, 3, 3, 2]
    
deltaT = duration / nsteps

# [temporal offset, refinement width, number of levels]
refinements = [[0.0,  0.028, 3.65],
               [0.15, 0.05,  1.3 ],
               [0.5,  0.10,  0.5 ]]

# Source, solution and refinement functions
def sourceAt(time):
    I, C = sourceIntensity(time), numpy.array(sourcePosition(time))[:, numpy.newaxis]
    A = (numpy.sqrt(2 * numpy.pi) * sigma)**(-len(C));
    S = 1 / (2 * sigma * sigma)
    
    return lambda xyz : I * A * numpy.exp(-S * numpy.sum((xyz - C)**2, axis=0))
    
def solutionAt(time):
    if time == 0.0:
        return lambda xyz : xyz[0] * 0.0 + u0

    # Parameters for integrating semi-analytical solution
    dt1, order = 0.01, 30 
    n = int(numpy.ceil(time / dt1))
    dt2 = time / n
    
    # Distribute Gauss-Legendre points on [0, time] divided into n intervals
    gaussT, gaussW = numpy.polynomial.legendre.leggauss(order)
    tau = numpy.array([(t + 1) / 2 * dt2 + i * dt2 for i in range(n) for t in gaussT])
    weights = numpy.tile(gaussW * dt2 / 2, n) 
    
    intensities, centers = sourceIntensity(tau), numpy.array(sourcePosition(tau))
    ndim = centers.shape[0]
    
    # Semi-analytical solution parameters for all integration points in time
    w = numpy.sqrt(sigma**2 + 2 * conductivity / capacity * (time - tau))
    magnitudes = intensities * weights / (capacity * (2 * numpy.pi)**(ndim / 2) * w**ndim)
    widths = -1 / (2 * w**2)
    
    # Compute distance to source center and evaluate exponential for all xyz and integration points in time
    squareDistance = lambda xyz : numpy.sum((xyz.T[..., numpy.newaxis] - centers)**2, axis=1)
    
    return lambda xyz : numpy.sum(magnitudes * numpy.exp(squareDistance(xyz) * widths), axis=1) + u0

def closestPointOnLineSegment(line0, line1, point):
    ap, ab = numpy.subtract(point.T, line0), numpy.subtract(line1, line0)
    d0, d1 = numpy.dot(ap, ab), numpy.dot(ab, ab)
    
    if numpy.sum(ab**2) < 1e-40:
        return (point.T - numpy.array(line0)).T, numpy.zeros(point.shape[1])
    
    r = d0 / d1
    projection = numpy.add(line0, ab * r[:, numpy.newaxis])
    projection[r < 0] = line0
    projection[r > 1] = line1
    return projection.T, numpy.clip(r, 0.0, 1.0)

def refinementLevelFunctionAt(time):
    def refinementLevel(xyz):
        points, tau = closestPointOnLineSegment(sourcePosition(time), sourcePosition(0.0), xyz);
        delay, distance = time * tau, numpy.sqrt(numpy.sum((points - xyz)**2, axis=0))
        levels = numpy.zeros(len(delay), dtype=int)
        
        # Interpolate sigma and level for delays in each refinement segment
        for (delay0, sigma0, depth0), (delay1, sigma1, depth1) in zip(refinements[:-1], refinements[1:]):
            mask = numpy.logical_and(delay < delay1, delay >= delay0)
            tau = (delay[mask] - delay0) / (delay1 - delay0);
            sigma = (1 - tau) * sigma0 + tau * sigma1;
            maxlevel = (1 - tau) * depth0 + tau * depth1;
            levels[mask] = numpy.round(maxlevel * numpy.exp( -distance[mask]**2 / (2 * sigma**2)))
                
        return levels;
    return refinementLevel

def discretizeAt(time):
    grid = mlhp.Grid(nelements, lengths)
    
    refinement1 = mlhp.refineWithLevelFunction(refinementLevelFunctionAt(time))
    refinement2 = mlhp.refineInsideDomain(mlhp.implicitSphere(sourcePosition(time), 0.005), 5)
    
    mlhp.refineGrid(grid, mlhp.refinementOr(refinement1, refinement2))
        
    degrees = [[degreesPerLevel[cell.level]]*3 for cell in grid.cells if mlhp.isLeaf(cell)]
    
    return mlhp.Basis(grid, degrees, mlhp.trunkspace)

# Initial mesh and basis
basis0 = discretizeAt(0.0)
dofs0 = mlhp.l2ProjectFunction(basis0, lambda x : u0)

# Initial error and number of dofs is zero
ndof = 0
# l2Norms = numpy.zeros(3)

# Postprocess initial solution
postprocessors = [
    mlhp.makeSolutionPostprocessor(dofs0, "NumericalSolution"),
    mlhp.makeFunctionPostprocessor(sourceAt(0.0), "Source"),
    # mlhp.makeFunctionPostprocessor(solutionAt(0.0), "AnalyticalSolution"),
    # mlhp.makeFunctionPostprocessor(refinementLevelFunctionAt(0.0), "RefinementLevel")
]

vtuInterval = 8
                  
mlhp.postprocessVtu(basis0, postprocessors, "linear_am_1.vtu", mlhp.pplus(1))

# Time integration
for istep in range(nsteps):
    t0, t1 = istep * deltaT, (istep + 1) * deltaT
    basis1 = discretizeAt(t1)

    print("Time step " + str(istep + 1) + " / " + str(nsteps) + " (" + 
          str(basis1.nelements) + " elements and " + 
          str(basis1.ndof)      + " dofs)", flush=True)
    
    # Domain integral
    dofs1 = numpy.zeros(basis1.ndof)
    
    K, F = mlhp.integrateTransientHeatSystem(basis0, basis1, capacityFunction, 
        conductivityFunction, sourceAt(t0), sourceAt(t1), theta, deltaT, dofs0)
        
    # Dirichlet boundaries (left, right, front, back, bottom)
    faces = mlhp.boundaryFaces(basis1, [(0, 0), (0, 1), (1, 0), (1, 1), (2, 0)])
    dirichlet = mlhp.projectOnFaces(basis1, faces, solutionAt(t1))

    K, F = mlhp.constrainStrongly(K, F, dirichlet)
    
    # Linear solution
    interior = scipy.sparse.linalg.cg(K, F, tol=1e-10, M=mlhp.invdiag(K))[0]
    
    dofs1 = mlhp.expandDirichlet(interior, dirichlet)
    
    # Postprocess time-step solution
    postprocessors = [
        mlhp.makeSolutionPostprocessor(dofs1, "NumericalSolution"),
        mlhp.makeFunctionPostprocessor(sourceAt(t1), "Source"),
        # mlhp.makeFunctionPostprocessor(solutionAt(t1), "AnalyticalSolution"),
        # mlhp.makeFunctionPostprocessor(refinementLevelFunctionAt(t1), "RefinementLevel")
    ]
    
    if (istep + 1) % vtuInterval == 0:
        fileIndex = int((istep + 1) / vtuInterval + 1)
        mlhp.postprocessVtu(basis1, postprocessors, "linear_am_" + str(fileIndex) + ".vtu", mlhp.pplus(1))
      
    # Integrate error
    ndof += basis1.ndof
    
    # factor = deltaT / 2.0 if istep + 1 == nsteps else deltaT
    # l2Norms += factor * mlhp.integrateL2Error(basis1, dofs1, solutionAt(t1), mlhp.pplus(1))**2
      
    basis0, dofs0 = basis1, dofs1
    
print("Average number of dofs: " + str(ndof / nsteps))
# print("Solution in L2 norm: " + str(numpy.sqrt(l2Norms[0])))
# print("Relative error in L2 norm: " + str(numpy.sqrt(l2Norms[2] / l2Norms[0]) * 100) + " %")


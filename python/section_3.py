# MIT License, Copyright (c) 2022, Philipp Kopp (philipp.kopp@tum.de)

import mlhp, numpy

# Create base mesh
mesh = mlhp.Grid([2, 2])

# Refine two levels
mesh.refine([0, 1, 3])
mesh.refine([7, 10, 12])

# Recreate data structure from paper
A = numpy.array([cell.neighbors for cell in mesh.cells])
P = numpy.array([cell.parent for cell in mesh.cells])
R = numpy.array([cell.level for cell in mesh.cells])
L = numpy.array([mlhp.isLeaf(cell) for cell in mesh.cells])

topology = numpy.stack([range(len(P)), *A.reshape([-1, 4]).T, P, R, L], axis=1)

print("Topology data:\n" + str(topology))

# Polynomials degrees for leaves / finite elements
p = numpy.array([[3, 3], [1, 1], [1, 2], [2, 1], [1, 2], [1, 1], 
                 [2, 1], [2, 1], [1, 2], [1, 1]] + [[1, 1]] * 12)

M = mlhp.createMlhpMasks(A, R, L, p, mlhp.tensorspace)
G = mlhp.createMlhpLocationMatrices(M, A, R)

ndof = numpy.max([numpy.max(Gi.ravel()) for Gi in G if Gi.size]) + 1

print("\nTensor-product masks:")
for Mi in M:
    print(Mi.T[::-1].astype(int))

print("\nLocation matrices (" + str(ndof) + " dofs):")
for Gi in G:
    print(Gi.T[::-1])
    
# Evaluate shape functions on element 3 at r, s = (0.3, -0.6)
N    = mlhp.evaluateLocal(mesh, M, mesh.fullIndices[3], (0.3, -0.6), (0, 0))
dNdx = mlhp.evaluateLocal(mesh, M, mesh.fullIndices[3], (0.3, -0.6), (1, 0)) 
dNdy = mlhp.evaluateLocal(mesh, M, mesh.fullIndices[3], (0.3, -0.6), (0, 1)) 

gradN = numpy.stack((dNdx, dNdy))

print("\nshape functions of element 3 at (0.3, -0.6):")
print(numpy.array2string(N, precision=4))

print("\nshape function derivatives of element 3 at (0.3, -0.6):")
print(numpy.array2string(gradN, precision=4))

# MIT License, Copyright (c) 2022, Philipp Kopp (philipp.kopp@tum.de)

import numpy
import scipy.sparse
import scipy.sparse.linalg

# Definitions to conform to the names in the paper
noValue = { numpy.dtype(bool) : False, numpy.dtype(int) : -1 }

productIndices = lambda limits: numpy.ndindex(*limits)
removeEntry = lambda V, i: V[:i] + V[i + 1:]
insertEntry = lambda V, i, v: V[:i] + (v, ) + V[i:]
sizes = lambda V: numpy.shape(V)
size = lambda V: len(V)
maxarray = lambda V0, V1: tuple(max(v0, v1) for v0, v1 in zip(V0, V1))
padding = lambda A, I: tuple((0, max(0, max(i, 1) + 1 - s)) for s, i in zip(sizes(A), I))
resize = lambda A, I: numpy.pad(A, padding(A, I), 'constant', constant_values=(None, noValue[A.dtype]))

def indexExists(i, s):
    e = True
    for sj, ij in zip(s, i):
        e = e and ij < sj
    return e
    
# -------------------------------- Algorithm 1 ---------------------------------

def operateOnInterface(A0, A1, n, op):
    s0 = sizes(A0)
    s1 = sizes(A1)
    sn0 = removeEntry(s0, n)
    sn1 = removeEntry(s1, n)
    
    for iN in productIndices(maxarray(sn0, sn1)):
        i0 = insertEntry(iN, n, 1)
        i1 = insertEntry(iN, n, 0)

        e0 = indexExists(i0, s0) 
        e1 = indexExists(i1, s1)

        v0 = A0[i0] if e0 else noValue[A0.dtype]
        v1 = A1[i1] if e1 else noValue[A1.dtype]

        r = op(v0, v1)
        
        if v0 != r and not e0:
            A0 = resize(A0, i0)
        if v1 != r and not e1:
            A1 = resize(A1, i1)
                
        if v0 != r:    
            A0[i0] = r
        if v1 != r:    
            A1[i1] = r
            
    return A0, A1

def operateOnInterfaces(A, N, op):
    for a in range(sizes(N)[1]):
        for i in range(N.shape[0]):
            Na = N[i, a, 1]
            if Na != -1:
                A[i], A[Na] = operateOnInterface(A[i], A[Na], a, op)

def createPfemMasks(N, p, maskInitializer, strategy='max_degree'):
        
    M = []
        
    for pe in p:
        # As in the paper (tensor-product space):
        #M.append(numpy.full(tuple(pi + 1 for pi in pe), True))
    
        # More generic (can also initialize with trunk space masks)
        M.append(maskInitializer(pe))

    op = numpy.logical_and if strategy == 'min_degree' else numpy.logical_or;
    
    for _ in range(sizes(N)[1] - 1):
        operateOnInterfaces(M, N, op)

    return M

# -------------------------------- Algorithm 2 ---------------------------------

def initializeGlobalIndices(M):
    G, nids = [], 0

    for i in range(size(M)):
        Mi = M[i]
        Gi = numpy.zeros(sizes(Mi), dtype=int)
        
        for j in productIndices(sizes(Mi)):
            Gi[j] = nids if Mi[j] else -1
            nids = nids + 1
            
        G.append(Gi)
            
    return G, nids

def removeUnassignedIndices(G, nids):
    exists = numpy.zeros(nids, dtype=bool)
    map = numpy.full(nids, -1, dtype=int)
    
    for Gi in G:
        for j in productIndices(sizes(Gi)):
            if Gi[j] != -1:
                exists[Gi[j]] = True
                
    nnew = 0
    for i in range(nids):
        if exists[i]:
            map[i] = nnew
            nnew = nnew + 1
            
    for Gi in G:
        for j in productIndices(sizes(Gi)):
            if Gi[j] != -1:
                Gi[j] = map[Gi[j]]

def returnFirstIndex(i0, i1):
    return i0

def createPfemLocationMatrices(M, N):
    G, nids = initializeGlobalIndices(M)
    
    operateOnInterfaces(G, N, returnFirstIndex)
    removeUnassignedIndices(G, nids)
    
    return G
    
# -------------------------------- Algorithm 3 ---------------------------------

def operateOnInternalInterfaces(A, N, L, op):
    for a in range(sizes(N)[1]):
        for i in range(size(A)):
            Na = N[i, a, 1]
            if Na != -1 and L[i] == L[Na]:
                A[i], A[Na] = operateOnInterface(A[i], A[Na], a, op)

def deactivateSlice(Mi, n, f):
    sn = removeEntry(sizes(Mi), n)
    
    for i_n in productIndices(sn):
        Mi[insertEntry(i_n, n, f)] = False

def deactivateOnInternalBoundaries(M, N, L):
    for a in range(sizes(N)[1]):
        for f in range(2):
            for i, Mi in enumerate(M):
                Na = N[i, a, f]
                if Na != -1 and L[i] != L[Na]:
                    deactivateSlice(Mi, a, f)
                    
def createMlhpMasks(N, L, E, p, maskInitializer):
    d, l = sizes(N)[1], 0
    M = []
    
    # Step 1: Initialize leaf masks
    l = 0
    for i, isleaf in enumerate(E):
        # As in the paper (tensor-product space):
        #M.append(numpy.full(tuple(pi + 1 for pi in p[l]), True))
    
        # More generic (can also initialize with trunk space masks)
        M.append(maskInitializer(p[l]) if isleaf else numpy.zeros([0]*d, dtype=bool))

        l = l + 1 if isleaf else l
        
    # Step 2: Restore compatibility by activating on neighbors
    for _ in range(d):
        operateOnInternalInterfaces(M, N, L, numpy.logical_or)
        
    # Step 3: Impose Dirichlet on internal boundaries
    deactivateOnInternalBoundaries(M, N, L)
    
    # Step 4: Restore compatibility by deactivating on neighbors
    for _ in range(d - 1):
        operateOnInternalInterfaces(M, N, L, numpy.logical_and)
    
    return M
    
def createMlhpLocationMatrices(M, N, L):
    G, nids = initializeGlobalIndices(M)
    
    operateOnInternalInterfaces(G, N, L, returnFirstIndex)
    removeUnassignedIndices(G, nids)
    
    return G

# ---------------------- Integrated Legendre polynomials -----------------------

def integratedLegendre(r, p, diff):

    # Precompute factors to speed up the evaluation
    if integratedLegendre.pmax < p:
        factorsJ = lambda j : (1 / j, 2 * j - 1, j - 1, 1 / numpy.sqrt(4 * j - 2))
        integratedLegendre.factors = [[]] + [factorsJ(j) for j in range(1, p + 1)]
        integratedLegendre.pmax = p

    I = numpy.zeros((p + 1, ) + numpy.shape(r))
    
    L  = [1, r, 0]
    
    if diff == 0:
        I[0] = 0.5 * (1.0 - r)
        I[1] = 0.5 * (1.0 + r)
        
        for j in range(2, p + 1):
            f1, f2, f3, f4 = integratedLegendre.factors[j] 

            L[2] = f1 * (f2 * r * L[1] - f3 * L[0])
            
            I[j] = f4 * (L[2] - L[0])
            
            L[0:2] = L[1:3]
    
    if diff == 1:
        I[0] = -0.5
        I[1] = 0.5
        
        dL = [0, 1, 0]
        
        for j in range(2, p + 1):
            f1, f2, f3, f4 = integratedLegendre.factors[j] 
            
            L[2] = f1 * (f2 * r * L[1] - f3 * L[0])
            dL[2] = f1 * (f2 * (L[1] + r * dL[1]) - f3 * dL[0])
            
            I[j] = f4 * (dL[2] - dL[0])
            
            L[0:2] = L[1:3]
            dL[0:2] = dL[1:3]

    return I.T

integratedLegendre.pmax = 0

# -------------------------------- Algorithm 4 ---------------------------------

# As in paper
def evaluateLocal(mesh, M, iL, r, k):
    N = []
    r = list(r)
    
    nL = 0
    i = iL
    while i != -1:
        Mi = M[i]
        s = sizes(Mi)
        
        if numpy.product(s) != 0:
            I = []
            
            for a in range(Mi.ndim):
                I.append(integratedLegendre(r[a], s[a] - 1, k[a]))
            
            for alpha in productIndices(s):
                if(Mi[alpha]):
                    Na = 0.5**(nL * sum(k))
                    for j in range(Mi.ndim):
                        Na = Na * I[j][alpha[j]]
            
                    N.append(Na)
                    
        for a in range(mesh.ndim):
            Na = mesh.cells[i].neighbors[a, 0]
            
            c = -1 if (Na == -1 or mesh.cells[Na].parent != mesh.cells[i].parent) else 1
            
            r[a] = (r[a] + c) / 2
        
        nL += 1
        i = mesh.cells[i].parent
        
    return numpy.array(N)

# More efficient evaluation that uses a grid of coordinates at once

def evaluateLocalGrid(mesh, masks, cell, rstVectors, diffIndices):
    rstVectors = [numpy.copy(rVector) for rVector in rstVectors]
    npoints = numpy.product(tuple(len(rVector) for rVector in rstVectors))
    maxdiff = numpy.max(diffIndices, axis=0)    
    ndof = mesh.reduceOverHierarchy(cell, lambda id: numpy.count_nonzero(masks[id]))
    N = numpy.empty((npoints, len(diffIndices), ndof))
    level, index0, index1 = 0, 0, 0
    
    while cell != -1:
        if masks[cell].size != 0:
            Mi = masks[cell]
        
            # Integrated legendre for all axes, diff orders and coordinates
            I = [[integratedLegendre(rVector, Mi.shape[ax] - 1, d) * 0.5**(d * level) 
                for d in range(maxdiff[ax] + 1)] for ax, rVector in enumerate(rstVectors)]
                
            # Slices to expand dimensions of polynomial axes for broadcasting into tensor-product
            slices = lambda ax : (numpy.newaxis, ) * ax + (slice(None), ) + (numpy.newaxis, ) * (Mi.ndim - ax - 1)
            
            for i, k in enumerate(diffIndices):
                # Full tensor product
                tensorProduct = I[0][k[0]][slices(0)*2]
                for ax in range(1, Mi.ndim):
                    tensorProduct = tensorProduct * I[ax][k[ax]][slices(ax)*2]
                    
                # Filter inactive entries and assign to correct range in N
                filteredTensorProduct = tensorProduct[(slice(None), ) * Mi.ndim + (Mi, )]
                index1 = index0 + filteredTensorProduct.shape[-1]
                N[:, i, index0:index1].flat = filteredTensorProduct.flat
                
            index0 = index1
        
        rstVectors = mesh.cells[cell].mapCoordinateGridToParent(rstVectors)
        cell, level = mesh.cells[cell].parent, level + 1
        
    return N

# ----------------------------- Hierarchical grid ------------------------------

class RootCell:
    def __init__(self, index, ncells, lengths, origin):
        self.id, self.parent, self.children, self.level = index, -1, -1, 0
        self.neighbors = numpy.full([len(ncells), 2], -1)
        ijk = numpy.unravel_index(index, ncells)
        
        for axis, i in enumerate(ijk):
            if i > 0:
                self.neighbors[axis, 0] = numpy.ravel_multi_index(
                    ijk[:axis] + (i - 1, ) + ijk[(axis + 1):], ncells)
            if i + 1 < ncells[axis]:
                self.neighbors[axis, 1] = numpy.ravel_multi_index(
                    ijk[:axis] + (i + 1, ) + ijk[(axis + 1):], ncells)
                   
        self.width = numpy.array([0.5 * l / n for l, n in zip(lengths, ncells)])
        self.center = numpy.array([w * (2 * i + 1) + o for i, w, o in zip(ijk, self.width, origin)])
    
    def mapCoordinateGridToParent(self, rst):
        pass
        
    def mapCoordinateGrid(self, rst, factor=1):
        shape = tuple(len(r) for r in rst)
        size = numpy.product(shape)
        
        J = numpy.repeat(numpy.diag(self.width * factor)[..., numpy.newaxis], size, axis=2)
        xyz = numpy.empty((len(rst), ) + shape)
        
        for ax, (r, c, w) in enumerate(zip(numpy.ix_(*rst), self.center, self.width)):
            xyz[ax] = r * w + c  
        
        return xyz.reshape(len(rst), -1), J
  
class OverlayCell:
    def __init__(self, index, position, parent):
        self.id, self.parent, self.children = index, parent.id,  -1
        self.position, self.level = position, parent.level + 1
        self.neighbors = numpy.copy(parent.neighbors)
        
    def mapCoordinateGridToParent(self, rst):
        return [(r + (1 if pos else -1)) / 2 for r, pos in zip(rst, self.position)]
  
isLeaf = lambda cell : numpy.shape(cell.children) == ()
  
def refineCell(cells, index):
    ndim = cells[index].neighbors.shape[0]
    children = numpy.zeros([2] * ndim, dtype=int)

    for ijk in numpy.ndindex(*([2]*ndim)):
        cells.append(OverlayCell(len(cells), ijk, cells[index]))
        children[ijk] = cells[-1].id
        
    for ijk in numpy.ndindex(*([2]*ndim)):
        for ax, pos in enumerate(ijk):
            other = ijk[:ax] + (1 - pos, ) + ijk[(ax + 1):]
            child = cells[children[ijk]]
            child.neighbors[ax, 1 - pos] = children[other]
            
            parentNeighbor = cells[index].neighbors[ax, pos]
            
            if parentNeighbor == -1 or isLeaf(cells[parentNeighbor]):
                child.neighbors[ax, pos] = parentNeighbor
            else:
                child.neighbors[ax, pos] = cells[parentNeighbor].children[other]
                cells[cells[parentNeighbor].children[other]].neighbors[ax, 1 - pos] = child.id
            
    cells[index].children = children
  
class Grid:
    def __init__(self, ncells, lengths=None, origin=None):
    
        lengths_ = numpy.array([1.0 for _ in ncells]) if lengths is None else lengths
        origin_ = numpy.array([0.0 for _ in ncells]) if origin is None else origin
        
        self.cells = [RootCell(i, ncells, lengths_, origin_) 
            for i in range(numpy.product(ncells))]
            
        self.ndim = len(ncells)
        self.fullIndices = numpy.arange(len(self.cells))

    def ncells(self):
        return len(self.cells)
    
    def nleaves(self):
        return len(self.fullIndices)

    def refine(self, fullIndices):
        for index in fullIndices:
            assert(isLeaf(self.cells[index]))
            refineCell(self.cells, index)
        self.fullIndices = [cell.id for cell in self.cells if isLeaf(cell)]   
            
    def mapCoordinateGrid(self, fullIndex, rst):
        rst = tuple(numpy.copy(r) for r in rst)
        factor = 1
        
        current, next = fullIndex, self.cells[fullIndex].parent
        while next != -1:
            rst = self.cells[current].mapCoordinateGridToParent(rst)
            factor *= 0.5
            current, next = next, self.cells[next].parent
        
        return self.cells[current].mapCoordinateGrid(rst, factor)
        
    def mapBasisFunctions(self, fullIndex, rstVectors, shapes, mapping=False):
        results = [shapes]
        
        if len(shapes) > 1 or mapping:
            results += [self.mapCoordinateGrid(fullIndex, rstVectors)]
            
        # Divide by diagonal of first Jacobian (mapping is constant)
        if len(shapes) > 1:
            results[0][1] /= numpy.diagonal(results[1][1][..., 0])[numpy.newaxis, :, numpy.newaxis]
            
        return results if mapping else results[0]
        
    def reduceOverHierarchy(self, fullIndex, function, binaryOp=None):
        op = (lambda a, b: a + b) if binaryOp is None else binaryOp

        result = function(fullIndex)
        fullIndex = self.cells[fullIndex].parent
        
        while fullIndex != -1:
            result = op(result, function(fullIndex))
            fullIndex = self.cells[fullIndex].parent
            
        return result
    
def leafIndices(grid):
    indices = numpy.full(grid.ncells(), -1, dtype=numpy.int64)
    
    index = 0
    for cell in grid.cells:
        if isLeaf(cell):
            indices[cell.id] = index
            index += 1
            
    return indices
    
# --------------------- Implicit functions and refinement ----------------------

implicitSphere = lambda center, radius : lambda xyz : numpy.sum((numpy.array(center) - xyz.T)**2, axis=1) <= radius**2

def pointMembershipTest(grid, icell, function, nseedpoints=5):
    rst = [numpy.linspace(-1.0, 1.0, nseedpoints)] * grid.ndim
    result = function(grid.mapCoordinateGrid(icell, rst)[0])
    outside, inside = not numpy.any(result), numpy.all(result)
    return -1 if outside else (1 if inside else 0)
   
def refineInsideDomain(function, maxdepth, nseedpoints=5):
    return lambda grid, icell: grid.cells[icell].level < maxdepth and \
        pointMembershipTest(grid, icell, function, nseedpoints) >= 0
    
def refineTowardsBoundary(function, maxdepth, nseedpoints=5):
    return lambda grid, icell: grid.cells[icell].level < maxdepth and \
        pointMembershipTest(grid, icell, function, nseedpoints) == 0   
        
def refineWithLevelFunction(levelFunction, nseedpoints=5):
    rst = [numpy.linspace(-1.0, 1.0, nseedpoints)]*3
    levels = lambda grid, icell : levelFunction(grid.mapCoordinateGrid(icell, rst)[0])
    return lambda grid, icell : grid.cells[icell].level < numpy.max(levels(grid, icell))
    
def refinementOr(*functions):
    return lambda grid, icell: numpy.any([function(grid, icell) for function in functions])
    
def refineGrid(grid, refine):
    begin, end = 0, grid.ncells( )
    while begin != end:
        grid.refine([icell for icell in range(begin, end) if refine(grid, icell)])
        begin, end = end, grid.ncells( )
    
# ---------------------------- Multi-level hp basis ----------------------------

def tensorspace(orders):
    return numpy.full(tuple(p + 1 for p in orders), True)

def trunkspace(orders):
    polynomialOrderAxes = [numpy.arange( q + 1 ) for q in orders]
    
    # Create nd-array with total polynomial degrees for each axis
    totalPolynomialOrders = numpy.sum(numpy.meshgrid(*polynomialOrderAxes, indexing='ij'), axis=0)
    
    # Create mask by thresholding the total degrees
    mask = totalPolynomialOrders < ( numpy.max( orders ) + 1 )
    
    # Copy slices (i, 0) to (i, 1) for each direction i
    for i in range(len(orders)):
        index0 = ( slice( None ), ) * i + ( 0, )
        index1 = ( slice( None ), ) * i + ( 1, )
        mask[index1] = mask[index0]
    
    return mask
    
# Accumulates basis data and helps evaluating basis-related information
class Basis:
    def __init__(self, grid, degrees, maskInitializer=trunkspace):
        A = numpy.array([cell.neighbors for cell in grid.cells])
        P = numpy.array([cell.parent for cell in grid.cells])
        R = numpy.array([cell.level for cell in grid.cells])
        L = numpy.array([isLeaf(cell) for cell in grid.cells])
        
        M = createMlhpMasks(A, R, L, degrees, maskInitializer)
        G = createMlhpLocationMatrices(M, A, R)
        
        self.grid = grid
        self.masks = M
        self.dofmaps = G
        self.ndof = numpy.max([numpy.max(Gi.ravel()) for Gi in G if Gi.size]) + 1
        self.nelements = grid.nleaves()
        
    def reduceOverHierarchy(self, leafIndex, function, binaryOp=None):
        return self.grid.reduceOverHierarchy(self.grid.fullIndices[leafIndex], function, binaryOp)
    
    def maxdegrees(self, leafIndex):
        function = lambda cell: tuple(s - 1 if s != 0 else 0 for s in self.masks[cell].shape)
        return self.reduceOverHierarchy(leafIndex, function, maxarray)
            
    def locationMap(self, leafIndex):
        function = lambda cell: [id for id in self.dofmaps[cell].ravel() if id != -1]
        return self.reduceOverHierarchy(leafIndex, function)
        
    def ndofelement(self, leafIndex):
        function = lambda cell: numpy.count_nonzero(self.masks[cell].ravel())
        return self.reduceOverHierarchy(leafIndex, function)
        
    def evaluate(self, leafIndex, rstVectors, diff, mapping=False):
        cell, ndim = self.grid.fullIndices[leafIndex], self.grid.ndim

        assert(diff <= 1)        
                
        shapes = []
        if diff == 0:
            shapes = [evaluateLocalGrid(self.grid, self.masks, cell, rstVectors, [(0, ) * ndim])[:,0,:]]
        if diff == 1:
            diffIndices = [[0] * ndim] + numpy.eye(ndim, dtype=int).tolist( )
            evaluation = evaluateLocalGrid(self.grid, self.masks, cell, rstVectors, diffIndices)
            shapes = [evaluation[:, 0, :], evaluation[:, 1:, :]]
          
        # # Paper implementation (much slower)
        # shapes = [[] for _ in range(diff + 1)]
        # for ijk in numpy.ndindex(*[len(s) for s in rstVectors]):
        #     rst = [rstVectors[ax][i] for ax, i in enumerate(ijk)]
        #     shapes[0].append(evaluateLocal(self.grid, self.masks, cell, rst, [0] * self.grid.ndim))  
        #     if(diff > 0):
        #         shapes[1].append([evaluateLocal(self.grid, self.masks, cell, rst, diff) 
        #             for diff in numpy.eye(self.grid.ndim)])
        # shapes = [numpy.array(N) for N in shapes]
        
        return self.grid.mapBasisFunctions(cell, rstVectors, shapes, mapping)

# ------------------------------ Sparse matrices -------------------------------

# Allocates a sparse matrix from given location maps in compressed row format
def allocateSparseMatrix(locationMaps, indextype=numpy.int64, datatype=numpy.float64) :

    globalNumberOfDofs = numpy.max([numpy.max(map) if len(map) else -1 for map in locationMaps]) + 1
    dofToElementCoupling = numpy.empty(globalNumberOfDofs, object)
    dofToElementCoupling[:] = [[] for _ in range(globalNumberOfDofs)]
    
    # "Invert" location maps: For each dof find the elements it has support in
    for ielement, locationMap in enumerate(locationMaps ) :
        for dofIndex in locationMap :
            dofToElementCoupling[dofIndex].append(ielement)

    numberOfCouplingElements = numpy.array([len(indices) for indices in dofToElementCoupling])
   
    interfaceDofs = numpy.where(numberOfCouplingElements > 1 )[0]
    internalDofs = numpy.where(numberOfCouplingElements == 1 )[0]
            
    # For internal dofs we just need to sort the location map of the corresponding element
    dofToElementCoupling[internalDofs] = [numpy.sort(locationMaps[indices[0]]) 
        for indices in dofToElementCoupling[internalDofs]]
   
    # For shared dofs concatenate location maps of adjacent elements and make them unique 
    dofToElementCoupling[interfaceDofs] = [numpy.unique(numpy.concatenate([locationMaps[ielement] 
        for ielement in elementIndices])) for elementIndices in dofToElementCoupling[interfaceDofs]]
                                            
    # Create sparse matrix
    K = scipy.sparse.csr_matrix((globalNumberOfDofs, globalNumberOfDofs))
    K.indptr = numpy.cumsum([0] + [len(indices) for indices in dofToElementCoupling], dtype=indextype)
    K.indices = numpy.concatenate([indices for indices in dofToElementCoupling if len(indices)], dtype=indextype)
    K.has_sorted_indices = 1
    
    # This seemes to be necessary as otherwise some of the lists above are still floating around
    dofToElementCoupling = None
    #gc.collect( )
 
    # Create zeros and force allocation
    K.data = numpy.empty(K.indices.shape, dtype = datatype )
    K.data[:] = 0.0
      
    return K
    
def applyStrongDirichlet(matrix, boundaryDofIndices):
    
    matrixSize = matrix.shape[0]
    numberOfRows = matrixSize - len(boundaryDofIndices)

    # Remove constrained rows
    remainingIndices = numpy.delete(numpy.arange(matrixSize), boundaryDofIndices)

    data, indices, indptr = matrix.data, matrix.indices, matrix.indptr
    
    matrix = scipy.sparse.csr_matrix((numberOfRows, matrixSize))
    matrix.indptr = numpy.insert(numpy.cumsum(indptr[remainingIndices + 1] - indptr[remainingIndices]), 0, 0)
    matrix.indices = numpy.empty(matrix.indptr[-1], dtype=numpy.dtype(indices[0]))

    for i, index in enumerate(remainingIndices):
        matrix.indices[matrix.indptr[i]:matrix.indptr[i+1]] = indices[indptr[index]:indptr[index + 1]]
    
    indices = None
    matrix.data = numpy.zeros(matrix.indptr[-1])
    
    for i, index in enumerate(remainingIndices):
        matrix.data[matrix.indptr[i]:matrix.indptr[i+1]] = data[indptr[index]:indptr[index + 1]]
    
    data, indptr = None, None
    matrix.has_sorted_indices=True
    
    # Do the rest
    B = matrix[:, boundaryDofIndices]
    matrix = matrix[:, remainingIndices]
    
    matrix.sort_indices()
    B.sort_indices()
    
    return matrix, B, remainingIndices
    
def constrainStrongly(matrix, rhs, dirichlet):
    K, B, indicesToKeep = applyStrongDirichlet(matrix, dirichlet[0])
    F = rhs[indicesToKeep] - B * dirichlet[1]
    return K, F
    
def constrainPenalty(K, F, dirichlet, penalty=1e10):
    K[dirichlet[0], dirichlet[0]] += penalty
    F[dirichlet[0]] += penalty * dirichlet[1]
    return K, F
    
def expandDirichlet(solution, dirichlet):
    activeMask = numpy.ones(len(solution) + len(dirichlet[0]), dtype=bool)
    activeMask[dirichlet[0]] = False
    
    U = numpy.zeros(len(activeMask))
    U[activeMask] = solution
    U[dirichlet[0]] = dirichlet[1]
    
    return U
    
allocateGlobalMatrix = lambda basis : allocateSparseMatrix([basis.locationMap(id) for id in range(basis.nelements)])
allocateGlobalVector = lambda basis : numpy.zeros([basis.ndof])
invdiag = lambda sparseMatrix : scipy.sparse.spdiags(1.0 / sparseMatrix.diagonal( ), 0, *sparseMatrix.shape)  

# Writes element matrix into sparse matrix in CSR format. May fail without error if matrix incorrect.
def assembleMatrix(globalMatrix, elementMatrix, locationMap):
    rowptrs, indices = globalMatrix.indptr, globalMatrix.indices
    search = lambda index : numpy.searchsorted(indices[rowptrs[index]:rowptrs[index+1]], locationMap) 
    sparseIndices = numpy.array([search(index) + rowptrs[index] for index in locationMap]).ravel( )
    globalMatrix.data[sparseIndices] += elementMatrix.ravel( )
    
def assembleVector(globalVector, elementVector, locationMap):
    globalVector[locationMap] = globalVector[locationMap] + elementVector

# --------------------------------- Assembly -----------------------------------

def evaluateSolution(N, dofs, locationMap):
    return numpy.dot(N, dofs[locationMap][:, numpy.newaxis]).ravel()

def evaluateDerivatives(dN, dofs, locationMap):
    return numpy.dot(dN, dofs[locationMap][:, numpy.newaxis]).reshape(dN.shape[:2]).transpose( )

def gaussLegendreProduct(accuracy):
    gauss = tuple(numpy.polynomial.legendre.leggauss(q) for q in accuracy)
    operands = tuple(entry for i, (ci, wi) in enumerate(gauss) for entry in (wi, (i, )))
    return tuple(c for c, _ in gauss), numpy.einsum(*operands, tuple(range(len(gauss)))).ravel( )
      
pplus = lambda n : lambda i, degrees : [p + n for p in degrees]
pplusIfNotLinear = lambda n : lambda i, degrees : [p + n if p > 1 else 1 for p in degrees] 

def integrateOnDomain(kernel, basis, maxdiff, quadrature=pplus(1)):
    for ielement in range(basis.nelements):
        rst, weights = gaussLegendreProduct(quadrature(ielement, basis.maxdegrees(ielement)))
        shapes, (xyz, J) = basis.evaluate(ielement, rst, maxdiff, mapping=True)
        weights *= numpy.linalg.det(J.transpose((2, 0, 1)))
        kernel(rst, xyz, weights, basis.locationMap(ielement), shapes)

singlepartition = lambda grid : (numpy.array((-1.0, 1.0)), ) * grid.ndim

# Append dimensions to array, e.g. shape (3, ) becomes (3, 1, 1) with ndim = 2
def expandDims(arr, ndim):
    return numpy.reshape(arr, numpy.shape(arr) + (1, ) * ndim)

# Compute element matrix as A^T * B * w
def computeLhs(A, B, w):
    axes = ((0, ), (0, )) if A.ndim == 2 else ((0, 1), (0, 1))
    return numpy.tensordot(A, B * expandDims(w, B.ndim - w.ndim), axes=axes)
    
# Compute element vector as A * w
def computeRhs(A, w):
    return numpy.sum(A * expandDims(w, 1), axis=0)   

def integratePoissonSystem(basis, k, f, quadrature=pplus(1)):
    K = allocateGlobalMatrix(basis)
    F = allocateGlobalVector(basis)

    def kernel(r, x, w, map, shapes):
        assembleMatrix(K, computeLhs(shapes[1], shapes[1], k(x) * w), map)
        assembleVector(F, computeRhs(shapes[0], f(x) * w), map)      
    
    integrateOnDomain(kernel, basis, 1, quadrature)
    
    return K, F
    
def integrateL2System(basis, m, f, quadrature=pplus(1)):
    M = allocateGlobalMatrix(basis)
    F = allocateGlobalVector(basis)

    def kernel(r, x, w, map, shapes):
        assembleMatrix(M, computeLhs(shapes[0], shapes[0], m(x) * w), map)
        assembleVector(F, computeRhs(shapes[0], f(x) * w), map)      
    
    integrateOnDomain(kernel, basis, 1, quadrature)
    
    return M, F
    
def l2ProjectFunction(basis, f, quadrature=pplus(1)):
    M, F = integrateL2System(basis, lambda x : 1, f, quadrature)
    return scipy.sparse.linalg.cg(M, F, tol=1e-8, M=invdiag(M))[0]
    
def addErrorNorms(norms, analytical, numerical, weights):
    norms[0] += 0.5 * numpy.sum(analytical**2 * weights)
    norms[1] += 0.5 * numpy.sum(numerical**2 * weights)
    norms[2] += 0.5 * numpy.sum((analytical - numerical)**2 * weights)

def integrateL2Error(basis, allDofs, solution, quadrature=pplus(3)):
    allDofs = numpy.array(allDofs)
    norms = [0.0, 0.0, 0.0]
    
    def kernel(r, x, w, map, shapes):
        numerical = evaluateSolution(shapes[0], allDofs, map)
        addErrorNorms(norms, solution(x), numerical, w)
        
    integrateOnDomain(kernel, basis, 0, quadrature)

    return numpy.sqrt(norms)

def integrateEnergyError(basis, allDofs, solutionDerivatives, quadrature=pplus(3)):
    allDofs = numpy.array(allDofs)
    norms = [0.0, 0.0, 0.0]
    
    def kernel(r, x, w, map, shapes):
        duN = evaluateDerivatives(shapes[1], allDofs, map)
        duA = solutionDerivatives(x)
        
        for analytical, numerical in zip(duA, duN):
            addErrorNorms(norms, analytical, numerical, w)
            
    integrateOnDomain(kernel, basis, 1, quadrature)

    return numpy.sqrt(norms)

# -------------- Project to new basis with different refinement ----------------

def intersectGatherChildren(grid, cell):
    if isLeaf(cell):
        return [(cell.id, singlepartition(grid))]
    else:
        map = lambda id, rst : grid.cells[id].mapCoordinateGridToParent(rst)
        return tuple((id, map(childId, rst)) for childId in cell.children.flat 
            for id, rst in intersectGatherChildren(grid, grid.cells[childId]))
             
def intersectCellsRecursive(grid0, grid1, cell0, cell1):
    if isLeaf(cell0):
        return tuple((cell0.id, rst, id, singlepartition(grid1)) 
            for id, rst in intersectGatherChildren(grid1, cell1))
    if isLeaf(cell1):
        return tuple((id, singlepartition(grid0), cell1.id, rst) 
            for id, rst in intersectGatherChildren(grid0, cell0))
        
    return tuple(entry for childId0, childId1 in zip(cell0.children.flat, cell1.children.flat)
        for entry in intersectCellsRecursive(grid0, grid1, grid0.cells[childId0], grid1.cells[childId1]))
    
def intersectGrids(grid0, grid1):
    nroots = numpy.searchsorted([cell.parent for cell in grid0.cells], -1, side='right')
    
    assert(grid0.ndim == grid1.ndim and nroots == numpy.searchsorted(
        [cell.parent for cell in grid0.cells], -1, side='right'))
    
    return tuple(entry for root0, root1 in zip(grid0.cells[:nroots], grid1.cells[:nroots])
                       for entry in intersectCellsRecursive(grid0, grid1, root0, root1))

def integrateBasisProjection(kernel, basis0, basis1, maxdiff, quadrature=pplus(1)):
    
    leafIndices0 = leafIndices(basis0.grid)
    partitions = [[] for _ in range(basis1.grid.ncells())]
    
    for id0, rst0, id1, rst1 in intersectGrids(basis0.grid, basis1.grid):
        partitions[id1].append((rst1, id0, rst0))
    
    map = lambda bounds, coords : [(c + 1)/2 * (b1 - b0) + b0 for (b0, b1), c in zip(bounds, coords)]
    
    for ielement1 in range(basis1.nelements):
        maxdegrees1 = basis1.maxdegrees(ielement1)
        partitions1 = partitions[basis1.grid.fullIndices[ielement1]]
        
        for rst1, id0, rst0 in partitions1:
            ielement0 = leafIndices0[id0]
            maxdegrees = maxarray(basis0.maxdegrees(ielement0), maxdegrees1)
            rstVectors, weights = gaussLegendreProduct(quadrature(ielement1, maxdegrees))
            rstVectors0, rstVectors1 = map(rst0, rstVectors), map(rst1, rstVectors)
            shapes0 = basis0.evaluate(ielement0, rstVectors0, maxdiff, mapping=False)
            shapes1, (xyz1, J1) = basis1.evaluate(ielement1, rstVectors1, maxdiff, mapping=True)
            weights1 = weights * numpy.linalg.det(J1.transpose((2, 0, 1))) * \
                                 numpy.product([(r[1] - r[0]) / 2 for r in rst1])
            map0, map1 = basis0.locationMap(ielement0), basis1.locationMap(ielement1)
            kernel(rstVectors0, rstVectors1, xyz1, weights1, map0, map1, shapes0, shapes1)


def integrateTransientHeatSystem(basis0, basis1, c, k, f0, f1, theta, deltaT, dofs0, quadrature=pplus(1)):
    K = allocateGlobalMatrix(basis1)
    F = allocateGlobalVector(basis1)

    def kernel(r0, r1, x, w, map0, map1, shapes0, shapes1):
        cValue, kValue = c(x) / deltaT, k(x)
        
        Me = computeLhs(shapes1[0], shapes1[0], w * cValue)
        Ke = computeLhs(shapes1[1], shapes1[1], w * (kValue * theta))
        
        u0 = evaluateSolution(shapes0[0], dofs0, map0)
        du0 = evaluateDerivatives(shapes0[1], dofs0, map0)
        
        Fe = computeRhs(shapes1[0], (cValue * u0 + (1 - theta) * f0(x) + theta * f1(x)) * w)
        
        for axis, du0i in enumerate(du0):
            Fe -= computeRhs(shapes1[1][:, axis, :], du0i * (1 - theta) * kValue * w)
        
        assembleMatrix(K, Ke + Me, map1)
        assembleVector(F, Fe, map1)      
    
    integrateBasisProjection(kernel, basis0, basis1, 1, quadrature)
    
    return K, F

# --------------------------- Boundary conditions ------------------------------

def allBoundaryFaces(basis):
    boundaries, leafIndex = [], 0
    for cell in basis.grid.cells:
        if isLeaf(cell):
            boundaries += [(leafIndex, axis, side) for axis in range(basis.grid.ndim) 
                for side in (0, 1) if cell.neighbors[axis, side] == -1]
            leafIndex += 1
    return boundaries

# For example, passing orientations ((0, 1), (1, 0)) returns all right and top boundary edges
def boundaryFaces(basis, orientations):
    return [face for face in allBoundaryFaces(basis) if face[1:] in orientations]
    
# Compute L2 projection of function on the given mesh faces
def projectOnFaces(basis, faces, function):
    if len(faces) == 0:
        return [], []

    reducedMaps, reducedLhs, reducedRhs = [], [], []
    dofMask = numpy.zeros(basis.ndof, dtype=bool)
    
    # Loop over boundary faces
    for ielement, axis, side in faces:
        
        # Distribute integration points on face
        accuracy = numpy.add(removeEntry(basis.maxdegrees(ielement), axis), 1)
        r, w = gaussLegendreProduct(accuracy)

        r = insertEntry(r, axis, numpy.array([-1.0 if side == 0 else 1.0]))
        w = 0.5 * w

        # Evaluate basis and compute L2 projection matrix and vector
        (N, ), (x, J) = basis.evaluate(ielement, r, 0, mapping=True)

        J[axis, :] = 0
        J[:, axis] = 0
        J[axis, axis] = 1

        w *= numpy.linalg.det(J.transpose((2, 0, 1)))
        
        Me, Fe = computeLhs(N, N, w), computeRhs(N, function(x) * w)
        
        # Filter basis functions that are zero on the boundary
        normM = numpy.linalg.norm(Me)
        filter = [numpy.linalg.norm(row) > 1e-8 * normM for row in Me]
        
        reducedMap = [dof for dof, nonzero in zip(basis.locationMap(ielement), filter) if nonzero]
        reducedMaps.append(reducedMap)
        reducedLhs.append(Me[numpy.ix_(filter, filter)])
        reducedRhs.append(Fe[filter])
        dofMask[reducedMap] = True
        
    # Eliminate basis function indices without contribution
    dofMap = numpy.cumsum(dofMask, dtype=numpy.int64) - 1
    reducedMaps = [[dofMap[index] for index in map] for map in reducedMaps]
    
    # Allocate system, assemble contributions and solve sparse system
    M = allocateSparseMatrix(reducedMaps)
    F = numpy.zeros(M.shape[0])
    
    for map, Me, Fe in zip(reducedMaps, reducedLhs, reducedRhs):
        assembleMatrix(M, Me, map)
        assembleVector(F, Fe, map)
    
    P = scipy.sparse.spdiags(1.0 / M.diagonal( ), 0, *M.shape)         
    dofValues = scipy.sparse.linalg.bicgstab(M, F, tol=1e-22)[0]
    
    return [numpy.nonzero(dofMask)[0], dofValues]
    
# ------------------------------ Postprocessing --------------------------------

# Postprocessor factory functions
def makeSolutionPostprocessor(dofs, name="Solution"): 
    def postprocessor(rst, xyz, J, shapes, locationMap, ielement):
        return evaluateSolution(shapes[0], dofs, locationMap)
    postprocessor.fieldname = name
    postprocessor.ncomponents = 1
    postprocessor.maxdiff = 0
    return postprocessor
       
def makeFunctionPostprocessor(function, name="Function", ncomponents=1):
    def postprocessor(rst, xyz, J, shapes, locationMap, ielement):
        return function(xyz)
    postprocessor.fieldname = name
    postprocessor.ncomponents = ncomponents
    postprocessor.maxdiff = -1
    return postprocessor
        

def evaluatePostprocessors(basis, postprocessors, sample=pplusIfNotLinear(2)):

    # Find out how many sample cells per element we need
    ncells = numpy.empty((basis.nelements, basis.grid.ndim), dtype=int)
    maxdiff = numpy.max([pp.maxdiff for pp in postprocessors])
    
    for ielement in range(basis.nelements):
        ncells[ielement] = sample(ielement, basis.maxdegrees(ielement))

    # Allocate data
    xyz = numpy.empty((numpy.sum(numpy.product(ncells + 1, axis=1)), basis.grid.ndim))
    data = [numpy.empty((xyz.shape[0], pp.ncomponents)) for pp in postprocessors]

    # Loop over elements, evaluate shape functions and then postprocessors
    index = 0
    for ielement, npoint in enumerate(ncells):
        rstVectors = [numpy.linspace(-1.0, 1.0, n + 1) for n in npoint]
        
        locationMap = basis.locationMap(ielement)
        shapes, (xyzI, J) = basis.evaluate(ielement, rstVectors, maxdiff, mapping=True)
        
        xyz[index : index + xyzI.shape[1]] = xyzI.T
        
        for d, pp in zip(data, postprocessors):
            d[index : index + xyzI.shape[1]].flat = pp(rstVectors, xyzI, J, shapes, locationMap, ielement).flat
        
        index += xyzI.shape[1]
        
    return xyz, data, ncells
    
def createVtuConnectivity(ncells):
    ids = numpy.arange(numpy.sum(numpy.product(ncells + 1, axis=1)), dtype=numpy.int64)
    ndim = ncells.shape[1]
    
    edges = numpy.empty((2 * (ndim - 1) * numpy.sum(ncells), 2), dtype=numpy.int64)
    volumes = numpy.empty((numpy.sum(numpy.product(ncells, axis=1)), 2**ndim), dtype=numpy.int64)
    cell0 = numpy.mgrid[(slice(2), )*ndim].transpose() .reshape(-1, ndim)
    
    def addEdges(nedges, axis, side0, side1=None):
        sides = (side0, side1) if ndim == 3 else (side0, )
        for iedge in range(nedges):
            ijk1 = insertEntry(sides, axis, iedge)
            ijk2 = insertEntry(sides, axis, iedge + 1)
            edges[edgeIndex + iedge] = (elementIds[ijk1], elementIds[ijk2])
        return nedges
    
    edgeIndex, volumeIndex, offset1 = 0, 0, 0
    for ielement, ncell in enumerate(ncells):
        
        offset0, offset1 = offset1, offset1 + numpy.product(ncell + 1)
        elementIds = numpy.reshape(ids[offset0:offset1], ncell + 1)
                
        # Internal cells
        for ijk0 in numpy.ndindex(*ncell):
            volumes[volumeIndex] = tuple(elementIds[tuple(ijk1)] for ijk1 in (cell0 + ijk0))
            volumeIndex += 1
            
        # Edge cells
        sides3D = ((0, 0), (0, -1), (-1, 0), (-1, -1))
        for sides in sides3D if ndim == 3 else ((0, ), (-1, )):
            for axis in range(ndim):
                edgeIndex += addEdges(ncell[axis], axis, *sides)
                
    assert(edgeIndex == edges.shape[0])
    assert(volumeIndex == volumes.shape[0])
    assert(offset1 == ids.shape[0])
        
    return edges, volumes

vtkDataTypeMap = { numpy.dtype('int8') : "Int8",  numpy.dtype('int16') : "Int16", 
                   numpy.dtype('int32') : "Int32", numpy.dtype('int64') : "Int64", 
                   numpy.dtype('float64') : "Float64" }

def writeUnstructuredMesh(filename, points, connectivity, offsets, types, pointdata, datanames):

    def writeAscii( text ):
        outfile = open( filename, "a" )
        outfile.write( text )
        outfile.write( "\n" )
        outfile.close( )
              
    def openNode( name, attributes = { } ):
        attributeString = ''.join( [" "+ key + "=\"" + value + "\"" for key, value in attributes.items( )] )  
        writeAscii( "<" + name + attributeString + ">" )
        return lambda : writeAscii( "</" + name + ">" )
        
    def writeDataSet( data, name=None ):
        attributes = { "format" : "ascii", "type" : vtkDataTypeMap[data.dtype], 
                       "NumberOfComponents" : str(data.shape[1] if data.ndim > 1 else 1) }
        if name != None:
            attributes["Name"] = name
            
        closeDatasetNode = openNode( "DataArray", attributes )
        writeAscii( '\n'.join( map( str, data.ravel() ) ) )
        closeDatasetNode( )

    headerAttributes = { "byte_order" : "LittleEndian",
                         "type"       : "UnstructuredGrid",
                         "version"    : "0.1" }
    
    open( filename, 'w').close( )
    writeAscii( "<?xml version=\"1.0\"?>" )
    closeVtkNode = openNode( "VTKFile", headerAttributes )
    closeGridNode = openNode( "UnstructuredGrid" )
    closePieceNode = openNode( "Piece", { "NumberOfCells"  : str( len( types ) ), 
                                          "NumberOfPoints" : str( points.shape[0] ) } )

    # Write all point data sets    
    closeNode = openNode( "PointData" )
    
    for data, name in zip(pointdata, datanames):
        writeDataSet(data, name)

    closeNode( )
    
    # Write result point coordinates
    closeNode = openNode( "Points" )
    writeDataSet( points )
    closeNode( )

    # Write result cells
    closeNode = openNode( "Cells" )
    writeDataSet( connectivity, "connectivity" )
    writeDataSet( offsets, "offsets" )
    writeDataSet( types, "types" )
    closeNode( )
    
    closePieceNode( )
    closeGridNode( )
    closeVtkNode( )
     
# Number codes for vertices, edges, quads and cubes
vtkCellTypeMap = { 0 : 1, 1 : 3, 2 : 8, 3 : 11 }

def postprocessVtu(basis, postprocessors, filename, sample=pplusIfNotLinear(2)):
    assert(basis.grid.ndim <= 3 and basis.grid.ndim >= 2)
    
    ndim = basis.grid.ndim
    xyz, data, ncells = evaluatePostprocessors(basis, postprocessors, sample)
    edges, volumes = createVtuConnectivity(ncells)
    
    offsets = numpy.zeros((len(volumes) + len(edges), ), dtype=numpy.int64)
    offsets[:len(volumes)] = (numpy.arange(len(volumes)) + 1) * 2**ndim
    offsets[len(volumes):] = (numpy.arange(len(edges  )) + 1) * 2 + len(volumes) * 2**ndim
    
    types = numpy.concatenate([numpy.full((volumes.shape[0], ), vtkCellTypeMap[ndim]),
                               numpy.full((edges.shape[0], ), vtkCellTypeMap[1])])
    
    connectivity = numpy.concatenate([volumes.ravel(), edges.ravel()])
    names = tuple(pp.fieldname for pp in postprocessors)
    
    xyz = numpy.concatenate([xyz, numpy.zeros((xyz.shape[0], 3 - xyz.shape[1]))], axis=1)
    
    writeUnstructuredMesh(filename, xyz, connectivity, offsets, types, data, names)
    
